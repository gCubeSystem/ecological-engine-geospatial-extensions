This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "ecological-engine-geospatial-extensions"


## [v1.5.3] - 2021-05-24

### Fixes

- Fixed obsolete short urls [#20971]


## [v1.5.2] 2021-01-18

### Features

 - update gis-interface lower bound range 
 - replaced repositories defined into the pom  with the new nexus url (nexus.d4science.org). update version to 1.5.2-SNAPSHOT
 - update ecological-engine-smart-executor lower range
 - removed maryTTS classes #20135


## [v1.5.1] [r4.24.0] - 2020-06-10

### Features

- Updated for support https protocol [#19423]


## [v1.4.0] - 2016-09-27

### Features

- Moved to Auth 2.0


## [v1.3.8] - 2016-04-01

### Features

- Better management of netcdf data



This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
